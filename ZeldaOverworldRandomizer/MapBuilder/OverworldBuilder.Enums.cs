﻿namespace ZeldaOverworldRandomizer.MapBuilder {
	public enum SettableTerrainType {
		None,
		Forest,
		Mountain,
		Graveyard,
		Desert,
		Coast,
		Lake
	}
}