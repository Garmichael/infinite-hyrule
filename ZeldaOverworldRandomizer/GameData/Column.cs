﻿using System.Collections.Generic;

namespace ZeldaOverworldRandomizer.GameData {
	public class Column {
		public readonly List<int> Tiles = new List<int>();
	}
}